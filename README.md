# FLLvsFER

Provides implementation of experiments described in:
* Romain Belmonte, Benjamin Allaert, Pierre Tirilly, Ioan Marius Bilasco, Chaabane Djeraba, et al.. Impact of Facial Landmark Localization on Facial Expression Recognition. IEEE Transactions on Affective Computing, Institute of Electrical and Electronics Engineers, In press, pp.1-1. ⟨10.1109/TAFFC.2021.3124142⟩


FLL vs FER code to be uploaded shortly

